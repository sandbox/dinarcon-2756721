CKEditor Widget
===============

Description
===========
This plugin introduces the Widget API. It is required by all plugins that
create widgets.

Widgets let you standardize, manage and control the layout and style of specific
sections of your WYSIWYG area, all this at the press of a button. They group
various elements together so you can easily move widgets around the surrounding
content.

Widgets are fully customizable because they are based on our plugin system.

Installation
============

This module requires the core CKEditor module.

1. Download the plugin from http://ckeditor.com/addon/widget.
2. Place the plugin in the root libraries folder (/libraries).
3. Enable CKEditor Widget module in the Drupal admin.

Uninstallation
==============
1. Uninstall the module from 'Administer >> Modules'.

MAINTAINERS
===========
Mauricio Dinarte - https://www.drupal.org/u/dinarcon

Credits
=======
Initial development and maintenance by Agaric.
